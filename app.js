'use strict'
var express = require('express');
var app = express();
//var port = process.env.PORT || 3000;
var bodyParser = require('body-parser');
var dotenv = require('dotenv');
var routes = require('./api/routes/duckRoutes');

dotenv.config();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.set('json spaces', 2);

routes(app);

// app.listen(port);
// console.log('Duck REST service started on: ' + port);

module.exports.app = app;