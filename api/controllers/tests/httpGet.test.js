const sinon = require('sinon');
const supertest = require('supertest');
const { Client } = require('pg');
const { app } = require('../../../app');
const getTestRequest = supertest(app);

let tupu = { id: 'ad5385bf-9d3c-4943-b6f4-0ab8cfbd07e9', name: 'Tupu Ankka' };

describe('GET DUCK', () => {

    beforeEach(() => {
        sandbox = sinon.createSandbox();
    });

    afterEach(() => {
        sandbox.restore();
    });

    it('get not found error', async done => {
        
        let getOneResult = {rows: []};
        sandbox.stub(Client.prototype, 'connect').returns();
        sandbox.stub(Client.prototype, 'query').resolves(getOneResult);
        sandbox.stub(Client.prototype, 'end').returns();
        
        await getTestRequest.get(`/ducks/${tupu.id}`)
            .then((response) => {
                expect(response.statusCode).toBe(404);
                done();
            });

        sandbox.assert.calledOnce(Client.prototype.connect);
        sandbox.assert.calledOnce(Client.prototype.query);
        sandbox.assert.calledOnce(Client.prototype.end);
    });

    it('get one duck', async done => {
        
        let getOneResult = { rows: [ tupu ] };
        sandbox.stub(Client.prototype, 'connect').returns();
        sandbox.stub(Client.prototype, 'query').resolves(getOneResult);
        sandbox.stub(Client.prototype, 'end').returns();
        
        await getTestRequest.get(`/ducks/${tupu.id}`)
            .then((response) => {
                expect(response.statusCode).toBe(200);
                expect(response.body.id).toBe(tupu.id);
                expect(response.body.name).toBe(tupu.name);
                done();
            });

        sandbox.assert.calledOnce(Client.prototype.connect);
        sandbox.assert.calledOnce(Client.prototype.query);
        sandbox.assert.calledOnce(Client.prototype.end);
    });

    it('get bad request error', async done => {
        
        sandbox.stub(Client.prototype, 'connect').returns();
        sandbox.stub(Client.prototype, 'query').rejects();
        sandbox.stub(Client.prototype, 'end').returns();
        
        await getTestRequest.get(`/ducks/${tupu.id}`)
            .then((response) => {
                expect(response.statusCode).toBe(400);
                done();
            });

        sandbox.assert.calledOnce(Client.prototype.connect);
        sandbox.assert.calledOnce(Client.prototype.query);
        sandbox.assert.calledOnce(Client.prototype.end);
    });
});