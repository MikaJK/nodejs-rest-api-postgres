const sinon = require('sinon');
const supertest = require('supertest');
const { Client } = require('pg');
const { app } = require('../../../app');
const getTestRequest = supertest(app);

let tupu = { id: 'ad5385bf-9d3c-4943-b6f4-0ab8cfbd07e9', name: 'Tupu Ankka' };
let hupu = { id: 'bd5385bf-9d3c-4943-b6f4-0ab8cfbd07e9', name: 'Hupu Ankka' };
let lupu = { id: 'cd5385bf-9d3c-4943-b6f4-0ab8cfbd07e9', name: 'Lupu Ankka' };

let ducks = [ tupu, hupu, lupu ];

describe('GET ALL DUCKS', () => {

    beforeEach(() => {
        sandbox = sinon.createSandbox();
    });

    afterEach(() => {
        sandbox.restore();
    });

    it('get zero ducks', async done => {
        
        let getAllResults = { rows: [] };
        sandbox.stub(Client.prototype, 'connect').returns();
        sandbox.stub(Client.prototype, 'query').resolves(getAllResults);
        sandbox.stub(Client.prototype, 'end').returns();
        
        await getTestRequest.get('/ducks')
            .then((response) => {
                expect(response.statusCode).toBe(200);
                expect(response.body.length).toBe(0);
                done();
            });

        sandbox.assert.calledOnce(Client.prototype.connect);
        sandbox.assert.calledOnce(Client.prototype.query);
        sandbox.assert.calledOnce(Client.prototype.end);
    });

    it('get three ducks', async done => {
        
        let getAllResults = { rows: ducks };
        sandbox.stub(Client.prototype, 'connect').returns();
        sandbox.stub(Client.prototype, 'query').resolves(getAllResults);
        sandbox.stub(Client.prototype, 'end').returns();
        
        await getTestRequest.get('/ducks')
            .then((response) => {
                expect(response.statusCode).toBe(200);
                expect(response.body.length).toBe(3);
                done();
            });

        sandbox.assert.calledOnce(Client.prototype.connect);
        sandbox.assert.calledOnce(Client.prototype.query);
        sandbox.assert.calledOnce(Client.prototype.end);
    });

    it('get bad request error', async done => {
        
        sandbox.stub(Client.prototype, 'connect').returns();
        sandbox.stub(Client.prototype, 'query').rejects();
        sandbox.stub(Client.prototype, 'end').returns();
        
        await getTestRequest.get('/ducks')
            .then((response) => {
                expect(response.statusCode).toBe(400);
                done();
            });

        sandbox.assert.calledOnce(Client.prototype.connect);
        sandbox.assert.calledOnce(Client.prototype.query);
        sandbox.assert.calledOnce(Client.prototype.end);
    });
});