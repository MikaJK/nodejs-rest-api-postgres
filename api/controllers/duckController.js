var uuidv4 = require('uuid/v4');
var { Pool } = require('pg');
var { queryAllDucks, queryDuck } = require('../databases/duckDatabase');

const username = process.env.DATABASE_USERNAME;
const password = process.env.DATABASE_PASSWORD;
const host = process.env.DATABASE_HOST
const database_name = process.env.DATABASE_NAME;

// const connection = `postgres://${username}:${password}@${host}/${database_name}`;
// console.log(`CONNECTION: ${connection}`);
// const client = new pg.Client(connection);

const pool = new Pool({
    connectionString: process.env.DATABASE_URL
    //connectionString: connection
});

async function get_duck(duck_id) {
    console.log(`GET DUCK`);
    const client = await pool.connect();
    const getQuery = 'SELECT * FROM ducks WHERE id = $1';
    return new Promise((resolve, reject) => {
        client.query(getQuery, [duck_id])
            .then(result => {
                resolve(result);
            })
            .catch(error => {
                reject(error);
            });
    });
}

exports.create_duck = async function(request, response) {
    console.log(`HTTP POST`);
    const client = await pool.connect();
    const createQuery = `INSERT INTO ducks (id, name) VALUES ($1, $2) returning *`;
    const values = [
        uuidv4(),
        request.body.name
    ];
    return new Promise((resolve, reject) => {
        client.query(createQuery, values)
            .then((result) => {
                resolve(response.status(201).send(result.rows[0]));
            })
            .catch((error) => {
                reject(response.status(400).send(error));
            });
    });
}

exports.get_all_ducks = async function(request, response) {
    console.log(`HTTP GET ALL`);
    //const client = await pool.connect();
    //client.connect();
    // const getAllQuery = 'SELECT * FROM ducks';
    // return new Promise((resolve, reject) => {
    //     client.query(getAllQuery)
    //         .then((result) => {
    //             resolve(response.status(200).send(result.rows));
    //         })
    //         .catch((error) => {
    //             reject(response.status(400).send(error));
    //         });
    // });

    try {
        const result = await queryAllDucks();
        return response.status(200).send(result);
    } catch(error) {
        return response.status(400).send(error);
    }
}

exports.get_duck = async function(request, response) {
    console.log(`HTTP GET`);
    // return new Promise((resolve, reject) => {
    //     get_duck(request.params.duck_id)
    //         .then(query_result => {
    //             if (query_result.rows[0]) {
    //                 resolve(response.status(200).send(query_result.rows[0]));
    //             } else {
    //                 resolve(response.status(404).send());
    //             }
    //         })
    //         .catch(query_error => {
    //             reject(response.status(400).send(query_error));
    //         });
    // });

    try {
        const result = await queryDuck(request.params.duck_id)
        if ( result.rows[0] ) {
            return response.status(200).send(result.rows[0]);
        } else {
            return response.status(404).send();
        }
    } catch(error) {
        return response.status(400).send(error);
    }
}

async function update_duck(duck_id, name) {
    console.log(`HTTP POST`);
    const client = await pool.connect();
    const updateQuery =`UPDATE ducks SET name=$1 WHERE id=$2 returning *`;
    const values = [
            name,
            duck_id
        ];
    return new Promise((resolve, reject) => {
        client.query(updateQuery, values)
            .then(result => {
                resolve(result.rows[0]);
            })
            .catch(error => {
                reject(error);
            });
    });
}

exports.update_duck = async function(request, response) {
    console.log(`HTTP POST`);
    return new Promise((resolve, reject) => {
        get_duck(request.params.duck_id)
            .then(query_result => {
                if (query_result.rows[0]) {
                    return new Promise((resolve, reject) => {
                        update_duck(query_result.id, request.body.name)
                            .then(update_result => {
                                resolve(response.status(200).send(update_result));
                            })
                            .catch(update_error => {
                                reject(response.status(400).send(update_error));
                            });
                    });
                } else {
                    resolve(response.status(404).send());
                }
                
            })
            .catch(query_error => {
                reject(response.status(400).send(query_error));
            })
    });
}

async function delete_duck(duck_id) {
    console.log(`DELETE DUCK`);
    const client = await pool.connect();
    const deleteQuery =`DELETE FROM ducks WHERE id=$1 returning *`;
    return new Promise((resolve, reject) => {
        client.query(deleteQuery, [duck_id])
            .then(result => {
                resolve(result.rows[0]);
            })
            .catch(error => {
                reject(error);
            });
    });
}

exports.delete_duck = async function(request, response) {
    console.log(`HTTP DELETE`);
    return new Promise((resolve, reject) => {
        get_duck(request.params.duck_id)
            .then(query_result => {
                if (query_result.rows[0]) {
                    return new Promise((resolve, reject) => {
                        delete_duck(query_result.id)
                            .then(delete_result => {
                                resolve(response.status(204).send(delete_result));
                            })
                            .catch(update_error => {
                                reject(response.status(400).send(update_error));
                            });
                    });
                } else {
                    resolve(response.status(404).send());
                }
                
            })
            .catch(query_error => {
                reject(response.status(400).send(query_error));
            })
    });
}
