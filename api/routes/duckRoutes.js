'use strict';
module.exports = function(app)
{
  var duck = require('../controllers/duckController');

  app.route('/ducks')
    .get(duck.get_all_ducks)
    .post(duck.create_duck);


  app.route('/ducks/:duck_id')
    .get(duck.get_duck)
    .put(duck.update_duck)
    .delete(duck.delete_duck);
};