var { Client } = require('pg');
var dotenv = require('dotenv');

dotenv.config();

const connection = process.env.DATABASE_URL;
const client = new Client(connection);

exports.queryAllDucks = async function() {
    console.log(`QUERY ALL DUCKS`);
    await client.connect();
    const getAllQuery = 'SELECT * FROM ducks';
    return new Promise((resolve, reject) => {
        client.query(getAllQuery)
            .then((result) => {
                resolve(result.rows);
            })
            .catch((error) => {
                reject(error);
            })
            .then(() => {
                client.end();
            });
    });
}

exports.queryDuck = async function(duck_id) {
    console.log(`QUERY ONE DUCK`);
    await client.connect();
    const getQuery = 'SELECT * FROM ducks WHERE id = $1';
    return new Promise((resolve, reject) => {
        client.query(getQuery, [duck_id])
            .then((result) => {
                resolve(result);
            })
            .catch((error) => {
                reject(error);
            })
            .then(() => {
                client.end();
            });
    });
}