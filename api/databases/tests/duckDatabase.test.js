const sinon = require('sinon');
const { Client } = require('pg');
const { queryAllDucks, queryDuck } = require('../duckDatabase');

let tupu = { id: 'ad5385bf-9d3c-4943-b6f4-0ab8cfbd07e9', name: 'Tupu Ankka' };
let hupu = { id: 'bd5385bf-9d3c-4943-b6f4-0ab8cfbd07e9', name: 'Hupu Ankka' };
let lupu = { id: 'cd5385bf-9d3c-4943-b6f4-0ab8cfbd07e9', name: 'Lupu Ankka' };

let ducks = [ tupu, hupu, lupu ];

describe('QUERY', () => {

    beforeEach(() => {
        sandbox = sinon.createSandbox();
    });

    afterEach(() => {
        sandbox.restore();
    });

    it('should get three ducks from stub', async () => {
        let queryResult = { rows: ducks };

        sandbox.stub(Client.prototype, 'connect').returns();
        sandbox.stub(Client.prototype, 'query').resolves(queryResult);
        sandbox.stub(Client.prototype, 'end').returns();

        const result = await queryAllDucks();
        expect(result).toBe(queryResult.rows);

        sandbox.assert.calledOnce(Client.prototype.connect);
        sandbox.assert.calledOnce(Client.prototype.query);
        sandbox.assert.calledOnce(Client.prototype.end);
    });

    it('should get one duck from stub', async () => {
        let queryResult = { rows: [ tupu ] };

        sandbox.stub(Client.prototype, 'connect').returns();
        sandbox.stub(Client.prototype, 'query').resolves(queryResult);
        sandbox.stub(Client.prototype, 'end').returns();

        const result = await queryDuck();
        expect(result).toBe(queryResult);

        sandbox.assert.calledOnce(Client.prototype.connect);
        sandbox.assert.calledOnce(Client.prototype.query);
        sandbox.assert.calledOnce(Client.prototype.end);
    });
});
